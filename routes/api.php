<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\SClassController;
use App\Http\Controllers\Api\SubjectController;
use App\Http\Controllers\Api\SectionController;
use App\Http\Controllers\Api\StudentController;
use App\Http\Controllers\AuthController;
 
Route::ApiResource('class', SClassController::class);

Route::ApiResource('subject', SubjectController::class);

Route::ApiResource('section', SectionController::class);

Route::ApiResource('/student', StudentController::class);


Route::group([

    'middleware' => 'api',
    'prefix' => 'auth'

], function ($router) {

    Route::post('login', [AuthController::class, 'login']);
    Route::post('logout', [AuthController::class, 'logout']);
    Route::post('refresh', [AuthController::class, 'refresh']);
    Route::post('me', [AuthController::class, 'me']);
    Route::post('register', [AuthController::class, 'register']);
    
});



