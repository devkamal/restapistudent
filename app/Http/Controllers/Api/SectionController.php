<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Section;

class SectionController extends Controller
{
    public function index(){
        $section = Section::all();
        return response()->json($section);
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'class_id' => 'required',
            'section_name' => 'required',
        ]);

        $section = Section::create($request->all());
        return response('Data inserted successfully!');

    }

    public function show($id){
        $section = Section::findorfail($id);
        return response()->json($section);
    }

    public function update(Request $request, $id){
        $updateData = Section::findorfail($id);
        $updateData->update($request->all());
        return response('Data updated success!');
    }


    public function destroy($id){
        Section::where('id', $id)->delete();
        return response('Data deleted');
    }


}
