<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Student;
use Illuminate\Support\Facades\Hash;
use DB;

class StudentController extends Controller
{

    public function index(){
        $student = Student::all();
        return response()->json($student);
    }

    public function store(Request $request){
        $validateData = $request->validate([
            'class_id' => 'required',
            'section_id' => 'required',
            'name' => 'required',
            'email' => 'required',
            'phone' => 'required',
            'password' => 'required',
            'address' => 'required',
            'photo' => 'required',
            'gender' => 'required',
        ]);

        $data = array();
        $data['class_id'] = $request->class_id;
        $data['section_id'] = $request->section_id;
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['password'] = Hash::make($request->password);
        $data['address'] = $request->address;
        $data['photo'] = $request->photo;
        $data['gender'] = $request->gender;
        DB::table('students')->insert($data);
        return response('Data inserted successfully!');

    }

    public function show($id){
        //Query Builder
        //$student = DB::table('students')->where('id',$id)->first();

        //Eloquent orm
        $student = Student::findorfail($id);
        return response()->json($student);
    }

    public function update(Request $request, $id){
        $data = array();
        $data['class_id'] = $request->class_id;
        $data['section_id'] = $request->section_id;
        $data['name'] = $request->name;
        $data['email'] = $request->email;
        $data['phone'] = $request->phone;
        $data['password'] = Hash::make($request->password);
        $data['address'] = $request->address;
        $data['photo'] = $request->photo;
        $data['gender'] = $request->gender;
        DB::table('students')->where('id',$id)->update($data);
        return response('Data updated successfully!');
    }

    public function destroy($id){
        $img = DB::table('students')->where('id',$id)->first(); //Get first data
        $image_path = $img->photo; //get only image data

        unlink($image_path); //image deleted from holder
        DB::table('students')->where('id',$id)->delete();
        return response('Student deleted');
    }


}
